# ACT-OUT

Write out an Adobe ACT file from CSV input.

This program will take a CSV file with "r", "g", and "b" columns and 256 rows,
and produce a binary ACT file as per the "Color Table" section of
<https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/#50577411_pgfId-1070626>

## usage

```text
act-out 0.1.1
Read a r,g,b .CSV to an Adobe .ACT

USAGE:
    act-out [FLAGS] [OPTIONS] --input <input>

FLAGS:
    -d, --debug      Activate debug mode
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --input <input>                  Input file name - a CSV with columns named "r", "g", and "b". Other columns
                                         will be ignored if present
    -o, --output <output>                Output file name [default: out.act]
    -t, --transparency <transparency>    Index of the value to use as transparency [default: 0]
```
