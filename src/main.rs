use serde_derive::Deserialize;
use std::{error::Error, fs::File, io, io::Write, path::PathBuf, process};
use structopt::StructOpt;

#[derive(Debug, Deserialize)]
struct RGB {
    r: u8,
    g: u8,
    b: u8,
}

/// A basic example
#[derive(StructOpt, Debug)]
#[structopt(about = "Read a r,g,b .CSV to an Adobe .ACT")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Input file name - a CSV with columns named "r", "g", and "b".
    /// Other columns will be ignored if present.
    #[structopt(short, long, parse(from_os_str))]
    input: PathBuf,

    /// Output file name.
    #[structopt(short, long, parse(from_os_str), default_value = "out.act")]
    output: PathBuf,

    /// Index of the value to use as transparency
    #[structopt(short, long, default_value = "0")]
    transparency: u8,
}

fn process(opt: Opt) -> Result<(), Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path(opt.input)?;
    let mut buf = Vec::with_capacity(256);
    for (i, row) in rdr.deserialize().enumerate() {
        let record: RGB = row?;
        if opt.debug {
            println!("{}: {:?}", i, record)
        };
        buf.push(record);
    }

    let len = buf.len();
    if len > 256 {
        return Err(Box::new(io::Error::new(
            io::ErrorKind::InvalidData,
            "input must have <=256 data rows",
        )));
    }

    let mut out = io::BufWriter::with_capacity(256 * 3 + 4, File::create(&opt.output)?);
    for record in buf {
        out.write_all(&[record.r, record.g, record.b])?;
    }
    for _ in len..256 {
        out.write_all(&[0, 0, 0])?;
    }
    out.write_all(&[(len >> 8) as u8, len as u8, 0, opt.transparency])?;
    out.flush()?;
    println!("Wrote {} RGB values to {:?}", len, &opt.output);
    Ok(())
}

fn main() {
    let opt = Opt::from_args();
    if let Err(err) = process(opt) {
        println!("ERROR: {}", err);
        process::exit(1);
    }
}
